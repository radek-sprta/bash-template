# bash-template

Bash template for writing script. Defines several useful functions, such as colored output, dependency check or error handling. 
It uses Google bash style with 4 space indents.

Deprecated by [bash-template-cookiecutter](https://gitlab.com/radek-sprta/bash-template-cookiecutter).
